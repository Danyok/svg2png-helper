#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void svg2pngs(QString filePath, QString fileName);

private slots:
    void on_pb_open_clicked();

    void on_pb_convert_clicked();

private:
    Ui::MainWindow *ui;

public:
    QString filePath,fileName;
};

#endif // MAINWINDOW_H
