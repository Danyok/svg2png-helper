#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QSvgRenderer>
#include <QPainter>
#include <QImage>

#include <QFileDialog>

#include <QDebug>

    #ifdef Q_OS_WIN32
    QString separator = "/";
    #endif
    #ifdef Q_OS_LINUX
    QString separator = "\";
    #endif

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::svg2pngs(QString filePath, QString fileName)
{
    QDir().mkdir(filePath+(fileName.left(fileName.size() - 4))+separator);

    QSvgRenderer renderer1(QString(filePath+fileName));
    QImage image1(172, 172, QImage::Format_ARGB32);
    QPainter painter1(&image1);
    renderer1.render(&painter1);
    QDir().mkdir(filePath+(fileName.left(fileName.size() - 4))+separator+"172x172");
    image1.save(filePath+(fileName.left(fileName.size() - 4))+separator+"172x172"+separator+(fileName.left(fileName.size() - 4))+".png",nullptr,100);

    QSvgRenderer renderer2(QString(filePath+fileName));
    QImage image2(128, 128, QImage::Format_ARGB32);
    QPainter painter2(&image2);
    renderer2.render(&painter2);
    QDir().mkdir(filePath+(fileName.left(fileName.size() - 4))+separator+"128x128");
    image2.save(filePath+(fileName.left(fileName.size() - 4))+separator+"128x128"+separator+(fileName.left(fileName.size() - 4))+".png",nullptr,100);

    QSvgRenderer renderer3(QString(filePath+fileName));
    QImage image3(108, 108, QImage::Format_ARGB32);
    QPainter painter3(&image3);
    renderer3.render(&painter3);
    QDir().mkdir(filePath+(fileName.left(fileName.size() - 4))+separator+"108x108");
    image3.save(filePath+(fileName.left(fileName.size() - 4))+separator+"108x108"+separator+(fileName.left(fileName.size() - 4))+".png",nullptr,100);

    QSvgRenderer renderer4(QString(filePath+fileName));
    QImage image4(86, 86, QImage::Format_ARGB32);
    QPainter painter4(&image4);
    renderer4.render(&painter4);
    QDir().mkdir(filePath+(fileName.left(fileName.size() - 4))+separator+"86x86");
    image4.save(filePath+(fileName.left(fileName.size() - 4))+separator+"86x86"+separator+(fileName.left(fileName.size() - 4))+".png",nullptr,100);
}

void MainWindow::on_pb_open_clicked()
{
    filePath = QFileDialog::getOpenFileName(this,
            tr("Open SVG"), "", tr("SVG Files (*.svg)"));
}

void MainWindow::on_pb_convert_clicked()
{
    if(filePath != nullptr)
    {
        qDebug()<<filePath;
        fileName = filePath.right(filePath.size()-filePath.lastIndexOf(separator)-1);
        filePath = filePath.left(filePath.size() - fileName.size());
        qDebug()<<filePath;
        qDebug()<<fileName;
        svg2pngs(filePath,fileName);
        fileName.clear();
        filePath.clear();
    }
    else
    {
        qDebug()<<"SVG not chosen.";
    }
}
